#Compute and print a list of fibonacci numbers upto n

def fibonacci(n):
    res = [] #list to store fibo numbers
    t1, t2 = 0, 1 #first two terms of the sequence
    while t1<n: #compute the next terms
        res.append(t1)
        t1, t2 = t2, t1+t2
    print('The terms of the fibonacci sequence are:')
    print(res)

def main():
    inp = input('Enter the upper bound for the fibonacci sequence:\n')
    try:
        num_terms = int(inp)
        return fibonacci(num_terms)
    except ValueError:
        print('Please enter an integer!')
        return main()

main()
