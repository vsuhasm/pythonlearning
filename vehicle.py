class Vehicle(object):
    """Class representing the parent class Vehicle
    for a car dealership. Car and Truck classses
    inherit from this class."""

    base_sale_price = 0.0
    wheels = 0

    def __init__(self, miles, make, model, year, sale_date):
        self.miles = miles
        self.make = make
        self.model = model
        self.year = year
        self.sale_date = sale_date

    def sale_price(self):
        """Returns the sale price for the vehicle"""
        if self.sale_date is not None:
            return 0.0
        return self.wheels*5000.0

    def purchase_price(self):
        """Returns the price that was paid for the car"""
        if self.sale_date is None:
            return 0.0
        return self.base_sale_price - (0.1*self.miles)

class Car(Vehicle):
    """Class representing car, inherits from Vehicle"""

    base_sale_price = 8000.0
    wheels = 4

    def vehicle_type(self):
        """Returns what type of vehicle this is"""
        return "Car"

class Truck(Vehicle):
    """Class representing Truck, inherits from Vehicle"""

    base_sale_price = 10000.0
    wheels = 4

    def vehicle_type(self):
        """Returns what type of vehicle this is"""
        return "Truck"

def print_info(obj):
    """Print the information from a vehicle object"""
    sold_on = obj.sale_date
    if sold_on is None:
        sale_p = obj.sale_price()
        purch_p = 0.0
        sold_on = "Not Sold"
    else:
        sale_p = 0.0
        purch_p = obj.purchase_price()

    break_line = ".........................................."
    print(break_line)
    print("Make: %s, Model: %s, Year: %s, Miles: %d, Sold: %s, Sale Price: %f, Purchase Price: %f" % (obj.make, obj.model,
                obj.year, obj.miles, sold_on, sale_p, purch_p))
    print(break_line)

def main():
    v1 = Car(5000, "Honda", "Civic", "2010", None)
    v2 = Truck(10000, "Leland", "A200", "2015", None)

    print_info(v1)
    print_info(v2)
    
    v1.sale_date = "May 25th, 2017"
    v2.sale_date = "May 27th, 2017"

    print_info(v1)
    print_info(v2)

    return 0

if __name__ == "__main__":
    main()
