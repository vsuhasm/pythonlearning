#Script to try out lambda expressions in python

def increment(n):
    return lambda x: x+n

def main():
    inp1 = input('Enter a number\n')
    inp2 = input('Increment it by?\n')
    try:
        x = float(inp1)
    except ValueError:
        print('Input is not a valid number!')
        return main()
    try:
        n = float(inp2)
        f = increment(n)
        print('Sum =', f(x))
    except ValueError:
        print('Input is not a valid number!')
        return main()

main()
    
